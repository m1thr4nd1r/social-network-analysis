# Adiciona a primeira linha (headers) em qualquer arquivo que de match no `ls`

	for i in `ls bolsonaro23_*`; do sed -i '1 i\Language;Tweet;Date' $i; done;

# Separar dados de um dia em um conjunto de dados com 1000 linhas
	# bolsonaro23_ é o prefixo usado nos nomes de arquivo

	split -l 1000 -d --additional-suffix ".csv" bolsonaro_23-10-2018.csv bolsonaro23_

# Lista linhas unicas e a quantidade de repetições de cada linha
	# basename -> pega o nome do arquivo (sem diretorio)
	# tail -> imprime tudo menos a primeira linha
	# cut -> seleciona apenas o segundo campo, delimitado por ";" (tweet)
	# sort -> ordena os textos
	# uniq -> mostra apenas (-d) quantidade (-c) de linhas repetidas junto da linha (precisa ter input ordenado)

	for i in `ls data/by-day/bolsonaro_*`; do j=`basename $i`; echo $j - $(tail -n +2 "data/by-day/$j" | cut -d";" -f 2 | sort | uniq -dc | awk '{total = total + $1}END{print "Repeated texts: " total}'); done;

# Conta quantidade de linhas no arquivo [SOMENTE BY-DAY]
	# for -> aplicado a todo elemento do `ls` (também define o nome do arquivo na saida)
	# wc -> conta a quantidade de linhas (-l) de um arquivo
	# awk -> printa a saida do comando anterior, formatada (a string é separada por espaço, $1 é a primeira string do array)
	
	for i in `ls data/by-day/bolsonaro_*`; do j=`basename $i`; wc -l "data/by-day/$j" | awk '{print $2" - "$1}'; done;

# Conta a quantidade de linhas de dois arquivos (Com e sem RT) e seu ratio [PRECISA ESTAR NO ROOT DO PROJETO]
	# ls -> pegar os arquivos que quer testar (tanto faz a pasta)
	# basename -> pega o nome do arquivo (sem diretorio)
	# wc -> conta as linhas (-l) dos dados completos e os sem RT
	# paste -> junta tudo numa linha
	# awk -> printa o resultado, formatado
 	
 	for i in `ls data/by-day/bolsonaro_*`; do j=`basename $i`; wc -l "data/by-day/$j" "data/by-day/without-rt/$j" | paste -s - | awk -v name=$j '{print name"- "$1" (All)\t"$3" (Without RT)\t"$3/$1*100"% (Reduction Ratio)"}'; done;

# COMBO BREAKKERRR [Calcula linhas do original e do sem RT, e ainda quantas linhas são repetidas em cada]

	for i in `ls data/by-day/bolsonaro_*`; do k=2; j=`basename $i`; echo $j $(wc -l "data/by-day/$j" | awk '{print $1}') $(tail -n +2 "data/by-day/$j" | cut -d";" -f $k | sort | uniq -dc | awk '{total = total + $1}END{print total}') $(wc -l "data/by-day/without-rt/$j" | awk '{print $1}') $(tail -n +2 "data/by-day/without-rt/$j" | cut -d";" -f $k | sort | uniq -dc | awk '{total = total + $1}END{print total}') | awk '{print $1" lines: All = "$2", "$3" repeated ["$3/$2*100"% from All]\t|\tWithout RTs = "$4" ["$4/$2*100"% from All], "$5" repeated ["$5/$3*100"% from All repeated, "$5/$4*100"% from Without RTs]"}'; done;
	
	# teste
	echo nome 4 3 2 1 | awk '{print $1" lines: All = "$2", "$3" repeated ["$3/$2*100"% from All]\t|\tWithout RTs = "$4" ["$4/$2*100"% from All], "$5" repeated ["$5/$3*100"% from All repeated, "$5/$4*100"% from Without RTs]"}'

# COMBO BREAKKER to CSV

	echo "name;date;total_lines;repeated_total_lines;without_rt;repeated_without_rt" | tee -a $(echo $j | cut -d"_" -f 1).ts; for i in `ls data/by-day/bolsonaro_*`; do k=2; j=`basename $i`; echo $(echo $j | cut -d"_" -f 1)";"$(echo $j | cut -d"_" -f 2 | cut -d"." -f 1)";"$(wc -l "data/by-day/$j" | awk '{print $1}')";"$(tail -n +2 "data/by-day/$j" | cut -d";" -f $k | sort | uniq -dc | awk '{total = total + $1}END{print total}')";"$(wc -l "data/by-day/without-rt/$j" | awk '{print $1}')";"$(tail -n +2 "data/by-day/without-rt/$j" | cut -d";" -f $k | sort | uniq -dc | awk '{total = total + $1}END{print total}') | tee -a $(echo $j | cut -d"_" -f 1).ts; done;

# Repete um push ate o sucesso

	while [[ "$(git push -u origin feature/updateData)" != 0 ]]; do echo "repeat"; done;