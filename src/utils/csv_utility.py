"""Module containing utilities for the csv files"""

def fix_columns(line, columns):
    """Fix amount of columns on a csv line"""
    import pycountry
    items = line.split(';')

    if (columns == 3 and len(items) < 3):
        items.insert(0, "und")
    elif (len(items) > columns):
        items[columns-2] = " . ".join(items[columns-2:-1])
        del(items[columns-1:-1])

    if (columns == 2):
        return ";".join(items)

    lang = pycountry.languages.get(alpha_2=items[0])
    if (items[0] != "und" and lang is None):
        items[1] = " . ".join(items[0:-1])
        items[0] = "und"

    return ";".join(items)

def fix_csv():
    """Standardize amount of columns in a csv file"""
    from utility import Utility
    import multiprocessing
    import pandas as pd
    from datetime import datetime
    from timeit import default_timer as timer

    pool = multiprocessing.Pool(Utility.CPU_QTD)
    for name in Utility.NAMES:
        filename = name.lower()
        columns = 3 if "bolsonaro" in filename else 2
        path = Utility.get_path(Utility.BASE_DATA_FOLDER, filename, "csv")
        print("Fixing {} ({})...\n\tCollapsing lines...".format(path, datetime.now()))

        total_timer = timer()
        parcial_timer = timer()
        inputs = []
        with open(path, encoding="utf8") as file:
            for line in file:
                items = line.split(';')
                while True:
                    try:
                        datetime.strptime(items[-1][:-4],"%Y-%m-%d %H:%M:%S")
                        break
                    except ValueError:
                        new_line = file.readline()
                        line = line[:-1] + " . " + new_line
                        items = new_line.split(';')
                inputs.append((line, columns))

        Utility.print_elapsed_time(parcial_timer, "\t...time elapsed %0.3fs.")
        print("\tAdjusting columns...")
        parcial_timer = timer()
        fixed_csv = pool.starmap(fix_columns, inputs)
        del(inputs)

        Utility.print_elapsed_time(parcial_timer, "\t...time elapsed %0.3fs.")
        print("\tWriting new file...")
        parcial_timer = timer()
        text = "Language;Tweet;Date\n" if columns == 3 else "Tweet;Date\n"
        text += "".join(fixed_csv)        
        with open(path, mode="w", encoding="utf8") as file:
            file.write(text)
        Utility.print_elapsed_time(parcial_timer, "\t...time elapsed %0.3fs.")
        Utility.print_elapsed_time(total_timer)

def split_by_day_and_rt():
    """Splits a csv on files by date and makes a copy without rt's"""
    from utility import Utility

    for name in Utility.NAMES:
        filename = name.lower()
        df = Utility.import_csv(Utility.get_path(Utility.BASE_DATA_FOLDER, filename, "csv"))
        print("\nSplitting {}...".format(name))

        for group_name, group in df.groupby(df.Date.dt.date):
            day_filename = "{}_{}".format(filename, group_name.strftime("%d-%m-%Y"))
            print("\tSplitting {}...".format(day_filename))
            file_path = Utility.get_path("{}/by-day".format(Utility.BASE_DATA_FOLDER), day_filename, "csv")
            group.to_csv(file_path, sep=Utility.SEPARATOR, header=True, index=False)
            print("\tDone...")
            
            print("\tSplitting {} without RT...".format(day_filename))
            file_path = Utility.get_path("{}/by-day/without-rt".format(Utility.BASE_DATA_FOLDER), day_filename, "csv")
            group_filtered = group[-group.Tweet.str.startswith("RT")]
            group_filtered.to_csv(file_path, sep=Utility.SEPARATOR, header=True, index=False)
            print("\tDone...")
        print("Done...")

def plot_ts_from_volume_data(columns=None, single_graph=False, extension="pdf"):
    """Plot data from a csv to a .pdf image"""
    from plotly.offline import plot
    import plotly.graph_objs as go
    import subprocess
    from utility import Utility

    # TODO: Improve orca installation check
    try:
        response = subprocess.check_output('orca -h', shell=True)
        if "plotly" not in str(response).lower() and (extension.contains("pdf") or extension.contains("eps")):
            raise subprocess.CalledProcessError
    except subprocess.CalledProcessError as ex:
        print("Incorrect orca installed, can't generate .pdf or .eps files (see: https://github.com/plotly/orca")
        return

    layout = go.Layout(
        font=dict(
            #family="Courier New, monospace",
            size=18,
            #color="#7f7f7f"
        ),   
        legend_orientation="h",
        #legend_title_text='Politician',
        legend=dict(
                    #0.1 (font 16) #0.05                            #1 (font 16)
            #x=0.5 - (0.1125 * len(columns) * (2 if single_graph else 1.3)), 
            x=0.5 - (0.15 * len(columns) * (2 if single_graph else 1.3)), # repeated
            y=1.175,  # 1.1 (font 16) # 1.075 (normal)
            #font=dict(
            #    family="sans-serif",
            #    size=18,
            #    color="black"
            #)
        ),
        margin=dict(
            l=25,
            r=25,
            b=10,
            t=10
            #"pad": 2,
        ),
        separators=",.",
        showlegend=True,
        yaxis=dict(
            ticks="outside",
            dtick=1000, #1000 p/ Lula no repeated, 10000 p/ Bolsonaro no repeated, o resto sem.            
            #"linewidth":2,
            #"linecolor":'black',
            #"gridwidth":2,
            #"gridcolor":'red'
        ),
        xaxis=dict(
        	dtick=1,
        ),
    )

    graph_data = list()

    for name in Utility.NAMES:
        df = Utility.import_csv(Utility.get_path(Utility.BASE_DATA_FOLDER, name.lower(), "csv"))
        #import code; code.interact(local = locals())
        if columns is None:
            columns = [x for x in range(2, len(df.columns))]

        for i in columns:
            graph_data.append(go.Scatter(
                x=df.date,
                y=df[df.columns[i]],
                name=df.name[0].title() if single_graph else df.columns[i].title().replace("_"," ")))

        if not single_graph:
            fig = go.Figure(data=graph_data, layout=layout)
            graph_data.clear()
            print("\nSaving {}.{}...".format(name, extension))
            if extension is not "html":
                fig.write_image(Utility.get_path("data/graphs/", name, extension))
            else:
                plot(fig,
                     filename=Utility.get_path("data/graphs/", name, extension),
                     auto_open=False)
            del(df,fig)

    if single_graph:
        fig = go.Figure(data=graph_data, layout=layout)
        print("\nSaving single.volume.{}...".format(extension))
        if extension is not "html":
            fig.write_image(Utility.get_path("data/graphs/", "single.volume", extension))
        else:
            plot(fig,
                filename=Utility.get_path("data/graphs/", "single.volume", extension),
                auto_open=False)
        del(df,graph_data,fig)

def start():
    #fix_csv()
    #split_by_day_and_rt()
    #plot_ts_from_volume_data([5], True)
    #plot_ts_from_volume_data([2,4]) # All and Without RTs
    plot_ts_from_volume_data([4,5]) # Without RTs and Repeated

if __name__ == '__main__':
    start()
    