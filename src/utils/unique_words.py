def start():
    """Start the data analysis"""
    import pandas as pd
    from utility import Utility
    from unidecode import unidecode
    from nltk.tokenize import word_tokenize
    from timeit import default_timer as timer
    import multiprocessing
    from datetime import datetime

    Utility.process_input()
    pool = multiprocessing.Pool(Utility.CPU_QTD)

    d = pd.DataFrame(columns=["Name", "Raw", "Cleaned", "Lemmatized", "Tokenized", "Without Stopwords", "Stemmized"])
    out_path = Utility.get_path("data/output", "unique-words", "csv")

    for name in Utility.NAMES:
        
        parcial_timer = timer()
        
        file_path = Utility.get_path("data", "{} - Pre".format(name), "csv")
        df = Utility.import_csv(file_path)

        file_path = Utility.get_path("data", "{} - Lem".format(name), "csv")
        df_Lemmatized = Utility.import_csv(file_path)

        df['Raw'] = pool.starmap(word_tokenize, [[x] for x in df['Tweet']])

        df['Cleaned'] = pool.starmap(word_tokenize, [[x] for x in df['Preprocessing']])

        df['Lemmatized'] = df_Lemmatized['Preprocessing']
        del(df_Lemmatized)

        df['Tokenized'] = pool.starmap(Utility.tokenizer, [[x] for x in df['Lemmatized']])

        df['Without Stopwords'] = pool.starmap(Utility.tokenization, [[x] for x in df['Lemmatized']])

        df['Stemmized'] = pool.starmap(Utility.stemmization, [[x] for x in df['Without Stopwords']])

        df['Lemmatized'] = pool.starmap(word_tokenize, [[x] for x in df['Lemmatized']])

        Utility.print_elapsed_time(parcial_timer, "\n{}: Data ready, time elapsed: %0.3fs".format(datetime.now().strftime("%d/%m - %H:%M:%S")))
        parcial_timer = timer()

        row = {
            "Name": name, 
            "Urls": sum([1 for x in range(len(df['Raw'])) if len(df['Raw'][x]) != len(df['Cleaned'][x])]),
            "Accent": sum([sum([1 for y in x if y != unidecode(y)]) for x in df['Raw']]),
            "Raw": len(set(df['Raw'].sum())), 
            "Cleaned": len(set(df['Cleaned'].sum())), 
            "Lemmatized": len(set(df['Lemmatized'].sum())), 
            "Tokenized": len(set(df['Tokenized'].sum())), 
            "Without Stopwords": len(set(df['Without Stopwords'].sum())), 
            "Stemmized": len(set(df['Stemmized'].sum())),
        }

        Utility.print_elapsed_time(parcial_timer, "\n{}: Summarized, time elapsed: %0.3fs.".format(datetime.now().strftime("%d/%m - %H:%M:%S")))
        d = d.append(row, ignore_index=True)
        d.to_csv(out_path, sep=";", mode="w", header=True)
        del (row)

if __name__ == '__main__':
    start()
