"""Utilities library"""
class Utility:
    """Utility class with shared methods and data"""

    # Deve ser todo minusculo
    NAMES = ["bolsonaro_sample_01", "bolsonaro_sample_02", "bolsonaro_sample_03", "bolsonaro_sample_04"]
    #NAMES = ["lula_01-10-2018","lula_02-10-2018","lula_03-10-2018",
    #         "lula_04-10-2018","lula_05-10-2018","lula_06-10-2018",
    #         "lula_07-10-2018","lula_08-10-2018","lula_09-10-2018",
    #         "lula_10-10-2018","lula_11-10-2018","lula_12-10-2018",
    #         "lula_13-10-2018","lula_14-10-2018","lula_15-10-2018",
    #         "lula_16-10-2018","lula_17-10-2018","lula_18-10-2018",
    #         "lula_19-10-2018","lula_20-10-2018","lula_21-10-2018",
    #         "lula_22-10-2018","lula_23-10-2018","lula_24-10-2018",
    #         "lula_25-10-2018","lula_26-10-2018","lula_27-10-2018",
    #         "lula_28-10-2018","lula_29-10-2018"]

    STEMMING = True
    DATA_SIZE = 'a'
    CPU_QTD = None
    PARALLEL = False

    LEMMATIZE_GROUP = 'a'
    LEMMATIZE_ARGS = ""
    #LEMMATIZE_ARGS = "-Xmx{}m".format(1024)

    BASE_DATA_FOLDER = "data"

    DO_PREPROCESS = True
    DO_LEMMATIZE = True
    DO_TOKENIZE = True

    SEPARATOR = ';'
    #SEPARATOR = ','
    LANGUAGE = 'portuguese'
    #LANGUAGE = 'english'

    @classmethod
    def import_csv(cls, path):
        """Imports a csv located at :path"""
        import pandas as pd
        import re

        print("Loading {}...".format(path))
        dataframe = pd.read_csv(path, sep=cls.SEPARATOR, index_col=False, infer_datetime_format=True, error_bad_lines=False, memory_map=True)

        if 'Date' in dataframe.columns:
            dataframe.Date = [date.replace(tzinfo=None) for date in pd.to_datetime(dataframe.Date)]
            dataframe.sort_values(by='Date', inplace=True)
            dataframe.reset_index(drop=True, inplace=True)

        if 'Unnamed: 0' in dataframe.columns:
            del dataframe['Unnamed: 0']

        if 'Tokens' in dataframe.columns:
            dataframe.Tokens = dataframe.Tokens.apply(cls.list_from_string)

        if 'Connectivity' in dataframe.columns:
            dataframe.Connectivity = [list(pd.to_datetime(
                sorted(re.findall(r'\'{1}(.[^\']*)\'{1}', window))))
                                      for window in dataframe.Connectivity]

        dataframe.dropna(how="all", inplace=True)
        try:
            print(dataframe.head())
        except UnicodeEncodeError as ex:
            print("Head print failed due to: {}. Continuing....".format(ex.reason))
        return dataframe

    @classmethod
    def list_from_string(cls, data):
        """Transform a string, representing a list, to a list object"""
        from ast import literal_eval

        if isinstance(data, str):
            correct_data = literal_eval(data)
            return correct_data

        return []

    @classmethod
    def stemmization(cls, words):
        """Stemmize a list of words"""
        from nltk.stem import SnowballStemmer

        stemmer = SnowballStemmer(cls.LANGUAGE, True)
        return [stemmer.stem(word) for word in words]

    @classmethod
    def tokenization(cls, doc):
        """Makes the tokenization of a text document, removing stopwords,
        while possibly stemming the resulting tokens"""
        import nltk

        stopwords = nltk.corpus.stopwords.words(cls.LANGUAGE)
        words = [word for word in cls.tokenizer(doc) if word not in stopwords]

        if cls.STEMMING:
            return cls.stemmization(words)

        return words

    @classmethod
    def tokenizer(cls, doc):
        """Tokenize a text document"""
        import re

        words = re.findall(r"\b\w{2,}\b", doc)
        return words

    @classmethod
    def get_path(cls, folder, name, extension=None):
        """Returns path to a named file under folder with the specified extension"""
        import os
        from os import path

        if not path.exists(folder):
            os.mkdir(folder)

        if extension:
            return "{}/{}.{}".format(folder, name, extension)
        return "{}/{}".format(folder, name)

    @classmethod
    def remove_file(cls, filepath):
        """Remove the file indicated on filepath"""
        import os
        from os import path

        if path.exists(filepath):
            os.remove(filepath)

    @classmethod
    def print_elapsed_time(cls, time, text=None):
        """Print elapsed time based on the timer"""
        from timeit import default_timer as timer

        if text is None:
            print("...time elapsed %0.3fs." % (timer() - time))
        else:
            print(text % (timer() - time))

    @classmethod
    def get_wordnet_pos(cls, word):
        """Map POS tag to first character lemmatize() accepts"""
        from nltk.corpus import wordnet
        import nltk

        tag = nltk.pos_tag([word])[0][1][0].upper()
        tag_dict = {"J": wordnet.ADJ,
                    "N": wordnet.NOUN,
                    "V": wordnet.VERB,
                    "R": wordnet.ADV}

        return tag_dict.get(tag, wordnet.NOUN)

    @classmethod
    def download_nltk_data(cls):
        """Download needed data from nltk"""
        import nltk

        nltk.download('wordnet', quiet=True)
        nltk.download('punkt', quiet=True)
        nltk.download('stopwords', quiet=True)

        # preprocessing.py
        nltk.download('averaged_perceptron_tagger', quiet=True)

    @classmethod
    def process_input(cls):
        """Process input from command line, in case it's different from default"""
        import sys

        param_qtd = len(sys.argv)

        if param_qtd > 1:
            _ = int(sys.argv[1])

        if param_qtd > 2:
            cls.NAMES = str(sys.argv[2]).split(",")

        if param_qtd > 3:
            cls.STEMMING = sys.argv[3]

        if param_qtd > 4:
            cls.DATA_SIZE = sys.argv[4]

        if param_qtd > 5:
            cls.SEPARATOR = sys.argv[5]

        if param_qtd > 6:
            cls.LANGUAGE = sys.argv[6]
