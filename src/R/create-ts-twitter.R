
rm(list=ls())
source("src/R/getNodes.R")
source("src/R/topological-overlapping.R")
source("src/R/timeseries.R")

##### testing #####

#subject="lula_"
#subject="bolsonaro_"
subject="bolsonaro_sample_"

twitter.files<-list.files(path="data/output/", pattern = paste(sep="", "^", subject, ".*csv"), full.names = T)
twitter.ts<-c()
for(i in 1:(length(twitter.files)-1))
{
  cat("***Comparing ", i, "..", i+1, "\n")
  day1<-processFile(twitter.files[i])  
  save(day1, file="data/output/day1.Rdata")
  day2<-processFile(twitter.files[i+1])  
  save(day2, file="data/output/day2.Rdata")
  
  graph.d1<-analyzing2days(day1, day2, dayGraph=1, save.file="data/output/gday1.list")
  save(graph.d1, file="data/output/gday1.Rdata")
  cat("***Graph 1 created \n")
  graph.d2<-analyzing2days(day1, day2, dayGraph=2, save.file="data/output/gday2.list")
  save(graph.d2, file="data/output/gday2.Rdata")
  cat("***Graph 2 created \n")
  
  rm(day1, day2)
  gc()
  
  twitter.ts[i]<-dist.graph(graph.d1, graph.d2)
  cat("***Distant Graph created \n")
  save(twitter.ts, file=paste(sep="", "data/output/p-", subject, "compnet-n0.ts"))
  
}

load(file=paste(sep="", "data/output/p-", subject, "compnet-n0.ts"))
name = unlist(str_split(subject, pattern="_"))[1]

#%%%%%
volume.subject<-read.table(paste(sep="", "data/", name, ".volume.csv"), sep=";", header=T)
mean.vector<-c();
for(i in 1:(min(length(volume.subject$without_rt), length(twitter.files))-1)){
  mean.vector[i]<-mean(volume.subject$without_rt[i:(i+1)])
}

# only for *_sample data
mean.vector = rep(c(3), times=length(twitter.files)) 

twitter.ts<-twitter.ts/mean.vector
#%%%%%

save(twitter.ts, file=paste(sep="", "data/output/p-", subject, "compnet-n0.ts"))
n.twitter.ts<-spline(x=twitter.ts, n = 2000)
foreign::write.arff(data.frame(tweets=n.twitter.ts$y), paste(sep="", "data/output/p-", subject, "compnet-n0.arff"))

pdf(file=paste(sep="", "data/output/", subject, "tnq.pdf"))
print(plotNovelty(twitter.ts, seq(as.Date("2018-10-02"), as.Date("2018-10-02") + length(twitter.ts) - 1, by = 1)))
dev.off()

pdf(file=paste(sep="", "data/output/", subject, "min.pdf"))
alert.points(twitter.ts, alert.function = "MIN", plot=T) #Why are some patters changing in a specific point?
dev.off()
pdf(file=paste(sep="", "data/output/", subject, "max.pdf"))
alert.points(twitter.ts, alert.function = "MAX", plot=T) #Why are some patters changing in a specific point?
dev.off()
pdf(file=paste(sep="", "data/output/", subject, "min-sp.pdf"))
alert.points(n.twitter.ts$y, alert.function = "MIN", plot=T) #Why are some patters changing in a specific point?
dev.off()
pdf(file=paste(sep="", "data/output/", subject, "max-sp.pdf"))
alert.points(n.twitter.ts$y, alert.function = "MAX", plot=T) #Why are some patters repeating in a specific point?
dev.off()
