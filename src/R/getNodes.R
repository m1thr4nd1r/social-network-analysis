library(dplyr)
library(stringr)
library(Matrix)
#library(pcalg)
library(readr)

getTerms<-function(line){
  output<-line %>% str_split(pattern = " ") %>% 
    unlist() %>% str_remove(pattern = "^'") %>%
    str_remove(pattern = "',") %>%
    str_remove(pattern = "'$")
  
  output
}

processFile<-function(filepath){
  con<-file(filepath, "r")
  resp<-list()
  all.lines<-c()
  nodes<-c()
  while (TRUE) {
    line<-readLines(con, n = 1)
    if(length(line) == 0){
      break
    }
    #linha<-str_extract(line, pattern = "\\[([^)]+)\\]") %>% 
    #  str_sub(start=2, end=-2) 
    sto.tok<-str_split(line, pattern = ";\\[")
    linha<-str_extract(sto.tok[[1]][length(sto.tok[[1]])], pattern = "([^)]+)\\]") %>% 
      str_sub(start=1, end=-2)
    if(!is.na(linha)){
      nodes<-c(nodes, getTerms(linha))
      all.lines<-c(all.lines, linha)
    }
  }
  close(con)
  resp$nodes<-unique(nodes)
  resp$lines<-all.lines
  resp
}

createMatrix<-function(info){
  weight<-matrix(0, nrow = length(info$nodes), ncol = length(info$nodes))
  colnames(weight)<-info$nodes
  rownames(weight)<-info$nodes
  
  for(i in 1:length(info$lines)){
    linha<-getTerms(info$lines[i]) %>% unique()
    #cat("-", i,"\n")
    #print(linha)
    if(length(linha)>1){
      for(j in 1:(length(linha)-1))
        for(k in (j+1):(length(linha))){
          weight[linha[j],linha[k]]=weight[linha[j],linha[k]]+1
        }
    }
  }
  #forceSymmetric(weight)
  #invisible(weight)
  weight[upper.tri(weight)]
}

createMatrix.file<-function(info, file=NA){

  if(is.na(file))
    file.name = runif(1, min=1000000000, max=9000000000)
  
  final.resp<-c()
  
  for(i in 1:(length(info$nodes)-1)){
    for(j in (i+1):length(info$nodes)){
      sum.value<-0
      for(k in 1:length(info$lines)){
        if(length(which((getTerms(info$lines[k]) %>% unique()) %in% info$nodes[i])) > 0)
          if(length(which((getTerms(info$lines[k]) %>% unique()) %in% info$nodes[j])) > 0)
            sum.value<-sum.value+1
      }
      final.resp<-c(final.resp, sum.value)
    }
  }
  final.resp
}

#createGraph<-function(info, edgeMat){
#  undgraph<-vector("list", length = length(info$nodes))
#  names(undgraph) <- info$nodes
#  for(i in 1:nrow(edgeMat)){
#    validConn<-which(edgeMat[i,]>0)
#    undgraph[[info$nodes[i]]] <- list(edges=names(validConn),weights=validConn)
#  }
#  gt <- new("graphNEL", nodes=info$nodes, edgeL=undgraph, edgemode="undirected")
#}

analyzing2days<-function(day1, day2, dayGraph=1, save.file=NA){
  resp<-list()
  resp$nodes<-c(day1$nodes, day2$nodes) %>% unique()
  if(dayGraph == 1){
    resp$lines<-day1$lines
  }else{
    resp$lines<-day2$lines
  }

  if(!is.na(save.file)){
  	save(resp, file=save.file)
  }
  
  invisible(createMatrix(resp))
}
