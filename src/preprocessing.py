"""Preprocess a given dataset with:
   decoding of unicode characters, removal of urls, lemmatization and tokenization"""

def preprocess(text):
    """Decodes (in UTF8) and removes links"""
    import re
    from unidecode import unidecode

    clean_text = unidecode(text).lower()

    # Melhorar: https://mathiasbynens.be/demo/url-regex
    regex = (r"(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}"
             r"www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))"
             r"[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})|\b(R|r)(T|t)\b|\b[0-9]+\b"
             # Removes links from tweets
             r"|\-\-") # Removes "--" from tweets

    clean_text = re.sub(regex, "", clean_text, flags=re.MULTILINE).strip()

    return clean_text

def lemmatize(text, filename=None):
    """Reduces texts to its basic gramatic version (i.e. went -> go)"""
    from utils.utility import Utility
    import tempfile
    from os import path
    import subprocess

    if Utility.LANGUAGE == 'portuguese':
        if filename:
            file_path = Utility.get_path("./modules/lemmatizer", str(filename))
            with open(file_path, "w") as file:
                file.write(text)
        else:
            with tempfile.NamedTemporaryFile(dir="./modules/lemmatizer", delete=False) as file:
                file.write(text.encode())
                filename = path.basename(file.name)
                file_path = file.name

        cmd = 'java -jar {} lematizador.jar {} nf'.format(Utility.LEMMATIZE_ARGS, filename)

        # subprocess.run nao eh compativel com versoes do python anteriores a 3.5
        response = subprocess.call(cmd, shell=True, cwd='modules/lemmatizer/')

        Utility.remove_file(file_path)
        Utility.remove_file("{}.tagged".format(file_path))
        Utility.remove_file("{}.mxp".format(file_path))
        file_path = "{}.out".format(file_path)

        if response != 0:
            Utility.remove_file(file_path)
            return text

        with open(file_path) as file:
            text = file.read().strip()

        Utility.remove_file(file_path)

    elif Utility.LANGUAGE == 'english':

        # import feito aqui para evitar problemas com o nltk/python na maquina clusterizada
        from nltk.stem import WordNetLemmatizer

        lemm = WordNetLemmatizer()
        words = [lemm.lemmatize(w, Utility.get_wordnet_pos(w))
                 for w in Utility.tokenizer(text)]
        text = ' '.join(words)

    return text

def preprocesser(texts):
    """Preprocesses texts"""
    from utils.utility import Utility
    import multiprocessing

    if Utility.PARALLEL:
        pool = multiprocessing.Pool(Utility.CPU_QTD)
        inputs = [[text] for text in texts]
        processed_texts = pool.starmap(preprocess, inputs)
        del inputs
    else:
        processed_texts = [preprocess(text) for text in texts]

    return processed_texts

def tokenizer(texts):
    """Tokenizes texts"""
    from utils.utility import Utility
    import multiprocessing

    if Utility.PARALLEL:
        pool = multiprocessing.Pool(Utility.CPU_QTD)
        inputs = [[text] for text in texts]
        tokens = pool.starmap(Utility.tokenization, inputs)
        del inputs
    else:
        tokens = [Utility.tokenization(text) for text in texts]

    return tokens

def lemmatizer(dataframe, name):
    """Splits dataframe in :size chunks and lemmatize them"""
    from utils.utility import Utility
    import multiprocessing

    if Utility.LEMMATIZE_GROUP == 'a':
        inputs = "\n<victormestrado>\n".join(dataframe.Preprocessing)
    elif Utility.LEMMATIZE_GROUP == 'd':
        df_filter = dataframe.Date.dt
        inputs = [("\n<victormestrado>\n".join(group.Preprocessing), group_name)
                  for group_name, group in dataframe.groupby(df_filter.date)]
    elif Utility.LEMMATIZE_GROUP == 'h':
        df_filter = dataframe.Date.dt
        inputs = [("\n<victormestrado>\n".join(group.Preprocessing),
                   "{}-T{}".format(group_name[0], group_name[1]))
                  for group_name, group in dataframe.groupby([df_filter.date, df_filter.hour])]
    elif Utility.LEMMATIZE_GROUP is None:
        inputs = [[text] for text in dataframe.Preprocessing]

    if Utility.PARALLEL:
        if Utility.CPU_QTD:
            pool = multiprocessing.Pool(Utility.CPU_QTD // 2)
        else:
            pool = multiprocessing.Pool(multiprocessing.cpu_count() // 2)

        lemmatized_text = [text
                           for texts in pool.starmap(lemmatize, inputs)
                           for text in texts.split("\n< victormestrado > \n")]
    else:
        lemmatized_text = lemmatize(inputs,name).split("\n< victormestrado > \n")

    del inputs
    return lemmatized_text

def data_split(data, size):
    """Limits the data that will be processed"""
    import datetime

    min_time = min(data.Date)
    target = max(data.Date)

    # Week / Day / Hour / Minute
    if size[-1] == 'w':
        target = min_time + datetime.timedelta(weeks=int(size[:-1]))
    elif size[-1] == "d":
        target = min_time.date() + datetime.timedelta(days=int(size[:-1]))
    elif size[-1] == 'h':
        target = min_time + datetime.timedelta(hours=int(size[:-1]))
    elif size[-1] == 'm':
        target = min_time + datetime.timedelta(minutes=int(size[:-1]))

    data = data.loc[data.Date <= target, :]

    return data

def export_csv(dataframe, filename, folder="data/output"):
    """Exports a dataframe to csv"""
    from utils.utility import Utility
    import traceback

    try:
        file_path = Utility.get_path(folder, filename, "csv")
        dataframe.to_csv(file_path, sep=Utility.SEPARATOR, header=True, index=False)
    except IOError:
        print("Failed to export preprocessed file.")
        traceback.print_exc()

    return dataframe

def start():
    """Ready preprocessing for execution"""
    from utils.utility import Utility
    from timeit import default_timer as timer

    Utility.process_input()
    Utility.download_nltk_data()
    total_timer = timer()

    for name in Utility.NAMES:
        name = name.lower()

        if Utility.DO_PREPROCESS:
            filename = name
        elif Utility.DO_LEMMATIZE:
            filename = "{} - Pre".format(name)
        elif Utility.DO_TOKENIZE:
            filename = "{} - Lem".format(name)
        else:
            filename = name

        person_df = Utility.import_csv(Utility.get_path(Utility.BASE_DATA_FOLDER, filename, "csv"))
        person_df = data_split(person_df, Utility.DATA_SIZE)

        parcial_timer = timer()

        if Utility.DO_PREPROCESS:
            filename = "{} - Pre".format(name)
            Utility.remove_file(Utility.get_path("data/output", filename, "csv"))
            print("\nPre-processing...")
            person_df['Preprocessing'] = preprocesser(person_df.Tweet)
            Utility.print_elapsed_time(parcial_timer)
            export_csv(person_df, "{} - Pre".format(name), Utility.BASE_DATA_FOLDER)
            parcial_timer = timer()

        if Utility.DO_LEMMATIZE:
            filename = "{} - Lem".format(name)
            Utility.remove_file(Utility.get_path("data/output", filename, "csv"))
            print("\nLemmatizing...")
            person_df['Preprocessing'] = lemmatizer(person_df, name)
            Utility.print_elapsed_time(parcial_timer)
            export_csv(person_df, "{} - Lem".format(name), Utility.BASE_DATA_FOLDER)
            parcial_timer = timer()

        if Utility.DO_TOKENIZE:
            filename = "{} - Tokenized".format(name)
            Utility.remove_file(Utility.get_path("data/output", filename, "csv"))
            print("\nTokenizing...")
            person_df['Tokens'] = tokenizer(person_df.Preprocessing)
            Utility.print_elapsed_time(parcial_timer)
            export_csv(person_df, "{} - Tokenized".format(name))

        Utility.print_elapsed_time(total_timer, "\nTotal time elapsed: %0.3fs.")
        del person_df

if __name__ == '__main__':
    start()
