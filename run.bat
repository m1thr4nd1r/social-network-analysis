@echo off
echo "Start preprocessing..."
pause
python src/preprocessing.py
echo "Preprocessing finished..."
echo "Extracting the time series..."
pause
rscript src/R/create-ts-twitter.R
echo "Processing finished."
pause