## Social Network Analysis



This project contains the final code for the approach developed during my master thesis.

### Executing


To run the python code (**Phase 2** of the approach), execute this command at the root of this repository:



```sh

python src/preprocessing.py

```



It's going to pickup the configuration (dataset files/folders) from src/utils/utility.py



Then you can execute the R code (**Phase 3 and 4** of the approach), with the following command:

```sh

rscript src/R/create-ts-twitter.R

```



In case you are wondering, **Phase 1** is done before coding so it is not included here :)



For more information, you can contact me or read [this](https://link.springer.com/chapter/10.1007/978-3-030-61380-8_44) paper

### Useful Links

[Pivot](https://towardsdatascience.com/pivot-tables-in-pandas-7834951a8177)

[Display Options, Assign, Apply](https://towardsdatascience.com/two-pandas-functions-you-must-know-for-easy-data-manipulation-in-python-2f6d0a2ef3e5)

[Presentation](https://towardsdatascience.com/pandas-presentation-tips-i-wish-i-knew-earlier-8e767365d190)

[Melt](https://towardsdatascience.com/reshape-pandas-dataframe-with-melt-in-python-tutorial-and-visualization-29ec1450bb02)