﻿#!/bin/bash
echo "Start preprocessing...\n"
read -n 1
python src/preprocessing.py
echo "\nPreprocessing finished...\nExtracting the time series...\n"
read -n 1
rscript src/R/create-ts-twitter.R
echo "\nProcessing finished."
read -n 1